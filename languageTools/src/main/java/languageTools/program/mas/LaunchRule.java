/**
 * The GOAL Grammar Tools. Copyright (C) 2014 Koen Hindriks.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package languageTools.program.mas;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * A launch rule, part of a MAS program, launches agents. Launch rules that are
 * conditional on the availability of entities in an environment only launch an
 * agent when an entity becomes available and connect the agent to that entity.
 */
public class LaunchRule {
	// Maximum number of applications of this rule; 0 means there is no maximum
	private int max = 0;
	// Required label of entity; empty means no label is required
	private String entityName = "";
	// Required type of the entity that agent is connected to; empty means no
	// type is required
	private String entityType = "";

	// Entity requirements
	private final Entity entity;
	// Launch instructions for launching agents
	private final List<LaunchInstruction> instructions;

	/**
	 * Creates a new launch rule.
	 *
	 * @param instructions
	 *            A list of launch instructions.
	 */
	public LaunchRule(Entity entity, List<LaunchInstruction> instructions) {
		this.entity = entity;
		this.instructions = instructions;
	}

	/**
	 * @return The entity requirements of this rule, or {@code null} if the rule is
	 *         unconditional.
	 */
	public Entity getEntity() {
		return this.entity;
	}

	/**
	 * @return The launch instructions of this rule.
	 */
	public List<LaunchInstruction> getInstructions() {
		return Collections.unmodifiableList(this.instructions);
	}

	/**
	 * @return {@code true} if entity requirements have been specified for rule.
	 */
	public boolean isConditional() {
		return this.entity != null;
	}

	@Override
	public String toString() {
		StringBuilder string = new StringBuilder();
		if (this.entityName != null && !this.entityName.isEmpty()) {
			string.append("when ");
			Map<String, String> description = new LinkedHashMap<>();
			if (this.entityType != null && !this.entityType.isEmpty()) {
				description.put("type", this.entityType);
			}
			description.put("name", this.entityName);
			if (this.max > 0) {
				description.put("max", Integer.toString(this.max));
			}
			if (description.isEmpty()) {
				string.append("entity");
			} else {
				string.append(description.toString());
			}
			string.append("@env do ");
		}
		string.append("launch ");
		if (this.instructions != null) {
			for (LaunchInstruction launch : this.instructions) {
				string.append(launch.toString());
			}
		}
		string.append(".");
		return string.toString();
	}
}
